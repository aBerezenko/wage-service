import React, {Component} from 'react'
import ApiService from "../service/ApiService";

class AddEmployeeComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            salary: ''
        };
        this.saveEmployee = this.saveEmployee.bind(this);
    }

    saveEmployee = (e) => {
        e.preventDefault();
        let employee = {
            name: this.state.name,
            salary: this.state.salary
        };
        ApiService.addEmployee(employee)
            .then(res => {
                this.setState({message: 'Employee added successfully.'});
                this.props.history.push('/staff');
            });
    };

    onChange = (e) =>
        this.setState({[e.target.name]: e.target.value});

    render() {
        return (
            <div>
                <h2 className="text-center">Add Employee</h2>
                <form>

                    <div className="form-group">
                        <label>Name:</label>
                        <input type="text" placeholder="Name" name="name" className="form-control"
                               value={this.state.name} onChange={this.onChange}/>
                    </div>

                    <div className="form-group">
                        <label>Salary:</label>
                        <input type="number" placeholder="salary" name="salary" className="form-control"
                               value={this.state.salary} onChange={this.onChange}/>
                    </div>

                    <button className="btn btn-success" onClick={this.saveEmployee}>Save</button>
                </form>
            </div>
        );
    }
}

export default AddEmployeeComponent;