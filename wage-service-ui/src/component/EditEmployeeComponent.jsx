import React, {Component} from 'react'
import ApiService from "../service/ApiService";

class EditEmployeeComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: '',
            name: '',
            salary: '',
        };
        this.saveEmployee = this.saveEmployee.bind(this);
        this.loadEmployee = this.loadEmployee.bind(this);
    }

    componentDidMount() {
        this.loadEmployee();
    }

    loadEmployee() {
        ApiService.fetchEmployeeById(window.localStorage.getItem("employeeId"))
            .then((res) => {
                let employee = res.data;
                this.setState({
                    id: employee.id,
                    name: employee.name,
                    salary: employee.salary,
                })
            });
    }

    onChange = (e) =>
        this.setState({[e.target.name]: e.target.value});

    saveEmployee = (e) => {
        e.preventDefault();
        let employee = {id: this.state.id, name: this.state.name, salary: this.state.salary};
        ApiService.editEmployee(employee)
            .then(res => {
                this.setState({message: 'Employee added successfully.'});
                this.props.history.push('/staff');
            });
    };

    render() {
        return (
            <div>
                <h2 className="text-center">Edit Employee</h2>
                <form>

                    <div className="form-group">
                        <label>Employee Name:</label>
                        <input type="text" placeholder="name" name="name" className="form-control" readOnly={true}
                               defaultValue={this.state.name}/>
                    </div>

                    <div className="form-group">
                        <label>Salary:</label>
                        <input type="number" placeholder="salary" name="salary" className="form-control"
                               value={this.state.salary} onChange={this.onChange}/>
                    </div>

                    <button className="btn btn-success" onClick={this.saveEmployee}>Save</button>
                </form>
            </div>
        );
    }
}

export default EditEmployeeComponent;