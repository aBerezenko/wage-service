import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import LoginComponent from './LoginComponent';
import AuthenticatedRoute from './AuthenticatedRoute';
import WageServiceApp from "../WageServiceApp";

class InstructorApp extends Component {
    render() {
        return (
            <>
                <Router>
                    <>
                        <Switch>
                            <Route path="/" exact component={LoginComponent} />
                            <Route path="/login" exact component={LoginComponent} />
                            <AuthenticatedRoute path="/wage-service" exact component={WageServiceApp} />
                        </Switch>
                    </>
                </Router>
            </>
        )
    }
}
export default InstructorApp