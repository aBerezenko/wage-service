import axios from '../axios/axios';

class ApiService {

    fetchStaff() {
        return axios.get('/staff');
    }

    fetchEmployeeById(id) {
        return axios.get('/employee/' + id);
    }

    deleteEmployee(id) {
        return axios.delete('/' + id);
    }

    addEmployee(employee) {
        return axios.post('/employee/create', employee);
    }

    editEmployee(employee) {
        return axios.put('/employee/' + employee.id, employee);
    }
    deleteStaff() {
        return axios.delete('/staff/delete');
    }
}

export default new ApiService();