pluginManagement {
    repositories {
        gradlePluginPortal()
    }
}

rootProject.name = "wage-service"

include("wage-service-server")
include ("wage-service-ui")

