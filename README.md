# spring-boot-react-js-wage-service-app 

## Description

Demo Web UI application with Spring Boot backend and React.js front-end. 

### Build and run

 * Build client by running `yarn build` inside "client" folder 
 * Start client by running `yarn start` inside "client" folder 
 * Build api by running `gradle clean build` inside "api" folder
 * Start api by running `gradle bootRun` inside "api" folder


 
