package org.demo;

import org.demo.model.Employee;
import org.demo.model.Salary;
import org.demo.repository.EmployeeRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EmployeeRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Test
    public void whenFindById_thenReturnEmployee() {
        // given
        Employee employee = new Employee("Test", new Salary(new BigDecimal(0)));
        entityManager.persist(employee);
        entityManager.flush();

        // when
        Optional<Employee> found = employeeRepository.findById(employee.getId());

        // then
        found.ifPresent(value -> Assert.assertEquals(value.getName(), employee.getName()));
    }
}
