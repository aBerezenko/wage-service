package org.demo;

import org.demo.model.Employee;
import org.demo.model.Salary;
import org.demo.repository.EmployeeRepository;
import org.demo.repository.SalaryRepository;
import org.demo.service.EmployeeService;
import org.demo.service.impl.EmployeeServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class EmployeeServiceTest {

    private final static String TEST_EMPLOYEE_NAME = "Test";
    private final static Long TEST_EMPLOYEE_ID = 1L;

    @TestConfiguration
    static class EmployeeServiceImplTestContextConfiguration {

        @MockBean
        private EmployeeRepository employeeRepository;

        @MockBean
        private SalaryRepository salaryRepository;

        @Before
        public void setUp() {
            Employee employee = new Employee( TEST_EMPLOYEE_ID,TEST_EMPLOYEE_NAME, new Salary(new BigDecimal(0)));

            Mockito.when(employeeRepository.findById(employee.getId()))
                    .thenReturn(Optional.of(employee));
        }

        @Bean
        public EmployeeService employeeService() {
            return new EmployeeServiceImpl(employeeRepository, salaryRepository);
        }
    }

    @Autowired
    private EmployeeService employeeService;

    @Test
    public void whenValidId_thenEmployeeShouldBeFound() {
        employeeService.findById(TEST_EMPLOYEE_ID)
                .ifPresent(employee -> Assert.assertEquals(employee.getName(), TEST_EMPLOYEE_NAME));
    }



}
