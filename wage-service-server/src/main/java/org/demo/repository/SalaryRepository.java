package org.demo.repository;

import org.demo.model.Salary;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface SalaryRepository extends JpaRepository<Salary, Long> {
}
