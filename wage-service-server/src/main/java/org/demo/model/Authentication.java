package org.demo.model;

import lombok.Data;
import lombok.NonNull;

@Data
public class Authentication {
    private @NonNull String message;
}
