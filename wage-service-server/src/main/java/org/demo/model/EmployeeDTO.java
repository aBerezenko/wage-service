package org.demo.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class EmployeeDTO {
    private @NonNull long id;
    private @NonNull String name;
    private @NonNull BigDecimal salary;
}
