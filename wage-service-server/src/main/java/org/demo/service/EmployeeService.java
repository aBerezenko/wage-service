package org.demo.service;

import org.demo.model.Employee;
import org.demo.model.Salary;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface EmployeeService {
    Employee createEmployee(String name, Salary salary);
    Optional<Employee> findById(Long id);
    Optional<Employee> update(long id, BigDecimal sum);
    List<Employee> getAllEmployees();
    void deleteAllEmployees();
}
