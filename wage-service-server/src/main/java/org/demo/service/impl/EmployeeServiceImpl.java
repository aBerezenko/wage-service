package org.demo.service.impl;

import lombok.RequiredArgsConstructor;
import org.demo.model.Employee;
import org.demo.model.Salary;
import org.demo.repository.EmployeeRepository;
import org.demo.repository.SalaryRepository;
import org.demo.service.EmployeeService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {
    private final EmployeeRepository employeeRepository;
    private final SalaryRepository salaryRepository;

    @Override
    public Employee createEmployee(String name, Salary salary) {
        Employee employee = new Employee(name, salary);
        return employeeRepository.save(employee);
    }

    @Override
    public Optional<Employee> findById(Long id) {
        return employeeRepository.findById(id);
    }

    @Override
    public Optional<Employee> update(long id, BigDecimal sum) {
        Optional<Salary> value = salaryRepository.findById(id);
        value.ifPresent(salary -> {
            salary.setSum(sum);
            salaryRepository.save(salary);
        });
        return employeeRepository.findById(id);
    }

    @Override
    public List<Employee> getAllEmployees() {
        List<Employee> list = new ArrayList<>();
        employeeRepository.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public void deleteAllEmployees() {
        employeeRepository.deleteAll();
    }

}
