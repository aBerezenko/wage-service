package org.demo.service.impl;

import org.demo.model.Employee;
import org.demo.model.EmployeeDTO;
import org.demo.service.DTOService;
import org.springframework.stereotype.Service;

@Service
public class DTOServiceImpl implements DTOService {
    @Override
    public EmployeeDTO convert(Employee emp) {
        return new EmployeeDTO(emp.getId(), emp.getName(), emp.getSalary().getSum());
    }
}
