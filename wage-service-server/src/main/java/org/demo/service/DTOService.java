package org.demo.service;

import org.demo.model.Employee;
import org.demo.model.EmployeeDTO;

public interface DTOService {
    EmployeeDTO convert(Employee emp);
}
