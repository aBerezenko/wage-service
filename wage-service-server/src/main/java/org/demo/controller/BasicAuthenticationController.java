package org.demo.controller;

import org.demo.model.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins={"http://localhost:3000"})
@RestController
public class BasicAuthenticationController {

    @GetMapping(path = "/basicauth")
    public Authentication authenticate() {
        return new Authentication("You are authenticated");
    }
}
