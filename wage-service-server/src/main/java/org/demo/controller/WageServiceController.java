package org.demo.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.demo.model.Employee;
import org.demo.model.EmployeeDTO;
import org.demo.model.Salary;
import org.demo.service.DTOService;
import org.demo.service.EmployeeService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
@RequestMapping("/api/v1/wage-service")
@RequiredArgsConstructor
@Slf4j
public class WageServiceController {
    private final EmployeeService employeeService;
    private final DTOService dtoService;

    @PostMapping("/employee/create")
    public ResponseEntity createEmployee(@RequestBody EmployeeDTO employee) {
        return ResponseEntity.ok(employeeService.createEmployee(employee.getName(), new Salary(employee.getSalary())));
    }

    @GetMapping("/employee/{id}")
    public ResponseEntity<EmployeeDTO> get(@PathVariable Long id) {
        Optional<Employee> employee = employeeService.findById(id);
        EmployeeDTO employeeDTO = new EmployeeDTO();
        if (employee.isPresent()) {
            employeeDTO = dtoService.convert(employee.get());
        }
        return ResponseEntity.ok(employeeDTO);
    }

    @PutMapping("/employee/{id}")
    public ResponseEntity<EmployeeDTO> update(@PathVariable Long id, @RequestBody EmployeeDTO employeeDTO) {
        Optional<Employee> employee = employeeService.update(id, employeeDTO.getSalary());
        if (!employee.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(dtoService.convert(employee.get()));
    }

    @GetMapping("/staff")
    public ResponseEntity<List<EmployeeDTO>> findAll() {
        List<Employee> employeeList = employeeService.getAllEmployees();
        List<EmployeeDTO> employeeDTOList = employeeList.stream().map(dtoService::convert).collect(toList());
        return ResponseEntity.ok(employeeDTOList);
    }

    @DeleteMapping("/staff/delete")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity delete() {
        employeeService.deleteAllEmployees();
        return ResponseEntity.ok().build();
    }
}
