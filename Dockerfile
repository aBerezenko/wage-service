FROM openjdk:13
VOLUME /tmp
COPY /build/libs/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]